
waypath.module
benjamin grant (bgrant@well.com)
may 2004

this module is distributed and licensed under the GNU public license agreement.



INSTRUCTIONS

1) install this to your modules directory

2) create the database table

3) add the contents of the css file to your theme's css file

4) add <div> to the list of elements accepted for nl2br call in filter.module
   alternatively you may go to the filter configuration page and ENABLE
   'strip tags' and then add '<div>' to the list of accepted tags.  also
   ENABLE style attributes to ensure that the HTML waypath sends is not
   stripped.


NOTE FOR DRUPAL 4.4.1 USERS

	* this module relies upon a nodeapi() hook that is not supported
	  in node.module under drupal 4.4.1.  to correct this add the
	  following line at line 479 of node.module:

		node_invoke_nodeapi($node, 'view', $main, $page);


TODO

	* support site inclusion and exclusion

	* fix nl2br issue triggered by filter.module and waypath's use of
	  div tags

	* convert to new install system when patches land

	* make applicable nodetypes configurable



CHANGES

june 2004
	* added support for api calls byDocId and byUrl
	  note form fields which will appear on node posting
	  form when byDocId or byUrl is configured
	


SUPPORT

	please file feature requests, bug reports, and patches via drupal.org

	all other questions etc -> bgrant@well.com


USEFUL?

	do you find this module useful?  please consider making a small
	donation to the children of san juan del sur, nicaragua.  more
	information can be found at:

		http://www.sanjuandelsur.org.ni/community/brugger/index.htm



